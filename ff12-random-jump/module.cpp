#include "pch.h"
#include "module.h"

const char* FF12HgetName()
{
	return "FF12 Random Jump Test";
}

tVersion FF12HgetVer()
{
	return {1,0,4};
}
