// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "hook.h"

HANDLE threadHandle;
volatile bool endThread = false;
DWORD WINAPI asyncThread(LPVOID lpParameter)
{
    volatile char* notInSaveLoad = (char*)0x215F1AE;
    do
    {
        if (*notInSaveLoad != 0 && (GetAsyncKeyState(VK_LSHIFT) & 0x8000) && (GetAsyncKeyState(VK_RSHIFT) & 0x8000))
        {
            LOG(INFO) << "Opening save menu";
            openfullscreenmenu_48d(0x8005, 0);
            *notInSaveLoad = 0;
            while (*notInSaveLoad == 0)
                Sleep(10);
        }
        Sleep(10);
    } while (!endThread);
    return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    DWORD threadID;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        LOG(INFO) << "Initializing random jump";
        srand(time(NULL));
        initHooks(allVbfHooks, _countof(allVbfHooks));
        threadHandle = CreateThread(NULL, NULL, asyncThread, NULL, NULL, &threadID);
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        endThread = true;
        WaitForSingleObject(threadHandle, INFINITE);
        CloseHandle(threadHandle);
        break;
    }
    return TRUE;
}

