#pragma once
struct tVersion
{
	int major;
	int minor;
	int step;
};

extern "C" const char* FF12HgetName();
extern "C" tVersion FF12HgetVer();