#include "pch.h"
#include "hook.h"

bool testHooks(const tHookInit* hooks, size_t count)
{
	for (size_t i = 0; i < count; i++)
	{
		if (memcmp((void*)hooks[i].callPtr, hooks[i].original, 5) != 0)
			return false;
	}
	return true;
}

bool initHooks(const tHookInit* hooks, size_t count)
{
	if (!testHooks(hooks, count))
	{
		LOG(INFO) << "Invalid game version. Unable to find proper hook values.";
		return false;
	}

	HANDLE hProcess = GetCurrentProcess();

	for (size_t i = 0; i < count; i++)
	{
		intptr_t hookAddr = intptr_t(hooks[i].hookPtr);
		int32_t callRelativeAddr = int32_t(hookAddr - (hooks[i].callPtr + 5));
		if (!WriteProcessMemory(hProcess, (LPVOID)(hooks[i].callPtr + 1), &callRelativeAddr, 4, NULL))
		{
			LOG(INFO) << "Unable to write " << hooks[i].name;
			return false;
		}
	}
	return true;
}

int mapJump(int mapNumber, int mapEntry, int animation, int one)
{
	int randomMapNumber = mapNumber, randomMapEntry = mapEntry;

	if (allMapEntries[mapNumber] != -2)
	{
		int random = rand() % _countof(mapEntries);
		randomMapNumber = mapEntries[random].mapNumber;
		randomMapEntry = mapEntries[random].mapEntry;
	}
	LOG(INFO) << "mapjump(" << mapNumber << ", " << mapEntry << ", " << animation << ") -> "
		<< "mapjump(" << randomMapNumber << ", " << randomMapEntry << ", " << animation << ")";

	return mapJumpHook(randomMapNumber, randomMapEntry, animation, one);
}
